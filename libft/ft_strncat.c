/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 03:44:29 by pmartin           #+#    #+#             */
/*   Updated: 2016/07/21 09:21:48 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strncat(char *s1, const char *s2, size_t nb)
{
	int		i;
	int		j;

	j = 0;
	i = ft_strlen(s1);
	while (j < (int)nb)
		s1[i++] = s2[j++];
	s1[i] = '\0';
	return (s1);
}
