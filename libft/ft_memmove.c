/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 23:45:50 by pmartin           #+#    #+#             */
/*   Updated: 2016/08/07 19:30:08 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			*ft_memmove(void *dst, const void *src, size_t len)
{
	char		*tdst;
	const char	*tsrc;
	char		*c;
	size_t		i;

	i = 0;
	tdst = dst;
	tsrc = src;
	if (!(c = (char *)malloc(sizeof(*c) * ft_strlen(src))))
		return (0);
	while (len--)
		c[i++] = *tsrc++;
	while (*c)
		*tdst++ = *c++;
	return (dst);
}
