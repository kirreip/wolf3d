/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/09 04:36:36 by pmartin           #+#    #+#             */
/*   Updated: 2014/11/14 08:38:06 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <limits.h>

static int	ft_countl(const char *s, char c)
{
	int i;
	int count;

	i = 0;
	count = 0;
	while (s[i] != '\0')
	{
		if (s[i] == c)
		{
			while (s[i] == c && s[i] != '\0')
				i++;
			if (s[i] == '\0')
				count--;
			else
				count++;
		}
		if (s[i] != '\0')
			i++;
	}
	return (count + 1);
}

static int	ft_countc(const char *s, char c)
{
	int i;

	i = 0;
	while (s[i] != c && s[i] != '\0')
		i++;
	return (i + 1);
}

char		**ft_strsplit(char const *s, char c)
{
	int		tab[8];
	char	**str;

	tab[7] = 0;
	while (tab[7] < 5)
		tab[tab[7]++] = 0;
	if (!(str = (char**)ft_strnew((ft_countl(s, c) + 1) * 10)))
		return (0);
	while (s[tab[0]] && tab[1] < (ft_countl(s, c)))
	{
		tab[2] = 0;
		while (s[tab[0]] == c)
			tab[0]++;
		if (!(str[tab[1]] = ft_strnew((ft_countc(&s[tab[0]], c) + 1))))
			return (0);
		while (s[tab[0]] != c && s[tab[0]] != '\0')
			str[tab[1]][tab[2]++] = s[tab[0]++];
		str[tab[1]][++tab[2]] = 0;
		(s[tab[0]]) ? tab[0]++ : (tab[0]);
		tab[1]++;
	}
	return (str);
}
