/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 22:45:53 by pmartin           #+#    #+#             */
/*   Updated: 2016/07/21 09:26:16 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strdup(char const *s)
{
	char	*s2;
	size_t	i;

	i = 0;
	if (!(s2 = (char *)malloc(sizeof(*s) * ft_strlen(s) + 1)))
		return (0);
	while (i <= ft_strlen(s))
	{
		s2[i] = s[i];
		i++;
	}
	s2[i] = '\0';
	return (s2);
}
