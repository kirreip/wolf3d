/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/09 00:06:58 by pmartin           #+#    #+#             */
/*   Updated: 2016/07/21 09:33:44 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memalloc(size_t size)
{
	size_t	i;
	void	*s;

	i = 0;
	if (!(s = malloc(size * sizeof(*((int*)s)))))
		return (0);
	while (i < size)
		((int*)s)[i++] = 0;
	return (s);
}
