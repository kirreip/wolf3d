/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 22:22:10 by pmartin           #+#    #+#             */
/*   Updated: 2014/11/12 03:26:08 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char		*truc(char *s)
{
	size_t	i;
	char	temp;

	i = 0;
	while (i < ft_strlen(s) - 1)
	{
		temp = s[i];
		s[i] = s[i + 1];
		s[i + 1] = temp;
		i++;
	}
	return (s);
}

static char		*ft_strrev(char *s)
{
	char	temp;
	int		i;
	int		j;

	i = 0;
	j = ft_strlen(s) - 1;
	if (s[0] == '-')
		s = truc(s);
	while (i < j)
	{
		temp = s[i];
		s[i++] = s[j];
		s[j--] = temp;
	}
	return (s);
}

char			*ft_itoa(int n)
{
	int		i;
	char	*s;

	i = 0;
	if (!(s = (char *)malloc(sizeof(*s) * 14)))
		return (0);
	if (n == -2147483648)
		return (ft_strcpy(s, "-2147483648"));
	if (n < 0)
	{
		s[i++] = '-';
		n = -n;
	}
	while (n > 9)
	{
		s[i++] = n % 10 + 48;
		n /= 10;
	}
	s[i++] = n + 48;
	s[i] = '\0';
	return (ft_strrev(s));
}
