/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/30 02:43:14 by pmartin           #+#    #+#             */
/*   Updated: 2016/11/22 14:15:31 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	var_init(t_var *var)
{
	var->sizex = 1000;
	var->sizey = 1000;
	ft_putendl("Init 1");
	var->cam.o.x = 15.11;
	var->cam.o.y = 37.11;
	var->cam.dir.x = 0;
	var->cam.dir.y = 1;
}

void	event_sdl(t_var *var)
{
	SDL_Event event;

	while (1)
	{
		if (!SDL_WaitEvent(&event))
			exit(NTM);
		var->event = event;
		if (var->event.window.event == SDL_WINDOWEVENT_CLOSE
			|| var->event.type == SDL_QUIT
			|| var->event.key.keysym.sym == SDLK_ESCAPE)
			break ;
		else
		{
			ft_key_hook(var);
			mainc(var);
		}
	}
	SDL_DestroyTexture(var->sdl_tex);
	SDL_DestroyWindow(var->sdl_win);
	SDL_Quit();
}

int		main(int ac, char **av)
{
	t_var var;

	if (ac == 1 && av)
	{
		var_init(&var);
		init(&var);
		init_map(&var);
		ft_putendl("Init OK");
		ft_putendl("Parser OK");
		mainc(&var);
		event_sdl(&var);
		ft_putendl("Fin Wolf3D");
	}
	return (0);
}
