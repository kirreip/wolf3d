/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mainc.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/24 17:09:59 by pmartin           #+#    #+#             */
/*   Updated: 2016/11/22 14:20:15 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void			ft_pixel_put(t_var *var, int k)
{
	int			color;

	if (k == 0)
		color = 0x0000ff;
	else if (k == 2)
		color = 0xffffff;
	else
	{
		if (!var->tool.side)
			color = (var->cam.o.x < var->tool.mapx) ? 0xff00ff : 0xffff00;
		else
			color = (var->cam.o.y < var->tool.mapy) ? 0xff0000 : 0x00ffff;
	}
	ft_memcpy(var->surface->pixels + (var->imgy)
			* var->surface->pitch
			+ var->imgx * 4, &color, 3);
}

void			makecolor(t_var *var, double d)
{
	double		k;

	k = var->sizey / d;
	var->imgy = 0;
	while (var->imgy < var->sizey)
	{
		if (var->imgy < (var->sizey - k) / 2)
			ft_pixel_put(var, 0);
		else if (var->imgy > var->sizey - ((var->sizey - k) / 2))
			ft_pixel_put(var, 2);
		else
			ft_pixel_put(var, 1);
		var->imgy++;
	}
}

void			mainc(t_var *var)
{
	init_viewplane(var);
	var->imgx = 0;
	while (var->imgx < var->sizex)
	{
		init_ray(var);
		init_dda(var);
		ez_dda(var);
		var->tool.dist = (!var->tool.side)
		? (var->tool.mapx - var->cam.o.x + (1 - var->tool.x) / 2) / var->ray.x
		: (var->tool.mapy - var->cam.o.y + (1 - var->tool.y) / 2) / var->ray.y;
		makecolor(var, var->tool.dist);
		var->imgx++;
	}
	SDL_DestroyTexture(var->sdl_tex);
	var->sdl_tex = SDL_CreateTextureFromSurface(var->sdl_ren, var->surface);
	SDL_RenderClear(var->sdl_ren);
	SDL_RenderCopy(var->sdl_ren, var->sdl_tex, 0, 0);
	SDL_RenderPresent(var->sdl_ren);
}
