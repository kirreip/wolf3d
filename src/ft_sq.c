/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sq.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 11:34:58 by pmartin           #+#    #+#             */
/*   Updated: 2016/11/22 13:08:24 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	ft_normvec(t_3d *vec)
{
	double d;

	d = sqrt(ft_sq(vec->x) + ft_sq(vec->y));
	vec->x /= d;
	vec->y /= d;
}

t_3d	ft_addvec3d(t_3d o, t_3d vec)
{
	o.x += vec.x;
	o.y += vec.y;
	return (o);
}

t_3d	ft_lenvec(t_3d vec, double l)
{
	vec.x *= l;
	vec.y *= l;
	return (vec);
}

void	ft_ezvec(t_3d *a, t_3d *b, t_3d *vec)
{
	vec->x = b->x - a->x;
	vec->y = b->y - a->y;
}

double	ft_sq(double k)
{
	return (k * k);
}
