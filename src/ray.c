/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/25 12:49:25 by pmartin           #+#    #+#             */
/*   Updated: 2016/11/22 13:08:41 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	init_viewplane(t_var *var)
{
	t_3d vec;

	ft_normvec(&var->cam.dir);
	var->viewplane = ft_addvec3d(var->cam.o, ft_lenvec(var->cam.dir, DIST));
	vec.x = var->cam.dir.y * -1;
	vec.y = var->cam.dir.x;
	var->viewplane = ft_addvec3d(var->viewplane, ft_lenvec(vec, WIDTH / 2.0));
}

void	init_ray(t_var *var)
{
	t_3d			tmp;
	static double	x;
	t_3d			vec;

	vec.x = var->cam.dir.y;
	vec.y = var->cam.dir.x * -1;
	if (!x)
		x = WIDTH / (double)var->sizex;
	tmp = ft_addvec3d(var->viewplane, ft_lenvec(vec, (double)(var->imgx) * x));
	ft_ezvec(&var->cam.o, &tmp, &var->ray);
	ft_normvec(&var->ray);
}
