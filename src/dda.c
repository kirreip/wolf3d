/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dda.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 04:20:42 by pmartin           #+#    #+#             */
/*   Updated: 2016/11/22 13:30:50 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void			init_dda(t_var *var)
{
	var->tool.mapx = (int)var->cam.o.x;
	var->tool.mapy = (int)var->cam.o.y;
	var->tool.dx = sqrt(1 + ft_sq(var->ray.y) / ft_sq(var->ray.x));
	var->tool.dy = sqrt(1 + ft_sq(var->ray.x) / ft_sq(var->ray.y));
	if (var->ray.x < 0)
	{
		var->tool.x = -1;
		var->tool.sx = (var->cam.o.x - var->tool.mapx) * var->tool.dx;
	}
	else
	{
		var->tool.x = 1;
		var->tool.sx = (var->tool.mapx + 1.0 - var->cam.o.x) * var->tool.dx;
	}
	if (var->ray.y < 0)
	{
		var->tool.y = -1;
		var->tool.sy = (var->cam.o.y - var->tool.mapy) * var->tool.dy;
	}
	else
	{
		var->tool.y = 1;
		var->tool.sy = (var->tool.mapy + 1.0 - var->cam.o.y) * var->tool.dy;
	}
}

void			ez_dda(t_var *var)
{
	while (1)
	{
		if (var->tool.sx < var->tool.sy)
		{
			var->tool.sx += var->tool.dx;
			var->tool.mapx += var->tool.x;
			var->tool.side = 0;
		}
		else
		{
			var->tool.sy += var->tool.dy;
			var->tool.mapy += var->tool.y;
			var->tool.side = 1;
		}
		if (map(var, var->tool.mapx, var->tool.mapy))
			break ;
	}
}
