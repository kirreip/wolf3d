/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 19:48:50 by pmartin           #+#    #+#             */
/*   Updated: 2016/11/21 21:36:18 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	init(t_var *var)
{
	if (SDL_Init(SDL_INIT_VIDEO))
		exit(NTM);
	ft_putendl("Init 1.5");
	var->sdl_win = SDL_CreateWindow("Wolf3d", SDL_WINDOWPOS_UNDEFINED,
								SDL_WINDOWPOS_UNDEFINED,
								var->sizex, var->sizey,
								SDL_WINDOW_SHOWN);
	ft_putendl("Init 2");
	var->sdl_ren = SDL_CreateRenderer(var->sdl_win, -1,
				SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	ft_putendl("Init 3");
	SDL_SetRenderDrawColor(var->sdl_ren, 0x00, 0x00, 0x00, 0x00);
	ft_putendl("Init 4");
	var->surface = SDL_CreateRGBSurface(0, var->sizex, var->sizey, 32,
									0, 0, 0, 0);
	ft_putendl("Init 5");
	var->sdl_tex = SDL_CreateTextureFromSurface(var->sdl_ren, var->surface);
	ft_putendl("Init 6");
}
