/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/14 22:15:13 by pmartin           #+#    #+#             */
/*   Updated: 2016/11/22 13:27:48 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void			ft_arrow(t_var *var)
{
	double		dx;
	double		dy;

	dx = var->cam.dir.x;
	dy = var->cam.dir.y;
	if (var->event.key.keysym.sym == SDLK_UP)
	{
		var->cam.o.x += (!map(var, (int)(var->cam.o.x + dx * FOR)
			, (int)var->cam.o.y)) ? dx * FOR : 0;
		var->cam.o.y += (!map(var, (int)var->cam.o.x
			, (int)(var->cam.o.y + dy * FOR))) ? dy * FOR : 0;
	}
	else if (var->event.key.keysym.sym == SDLK_DOWN)
	{
		var->cam.o.x -= (!map(var, (int)(var->cam.o.x - dx * FOR)
			, (int)var->cam.o.y)) ? dx * FOR : 0;
		var->cam.o.y -= (!map(var, (int)var->cam.o.x
			, (int)(var->cam.o.y - dy * FOR))) ? dy * FOR : 0;
	}
}

int				ft_key_hook(t_var *var)
{
	double		dx;
	double		dy;

	dx = var->cam.dir.x;
	dy = var->cam.dir.y;
	if (var->event.key.keysym.sym == SDLK_RIGHT)
	{
		var->cam.dir.x = dx * cos(-ROT) - dy * sin(-ROT);
		var->cam.dir.y = dx * sin(-ROT) + dy * cos(-ROT);
	}
	else if (var->event.key.keysym.sym == SDLK_LEFT)
	{
		var->cam.dir.x = dx * cos(ROT) - dy * sin(ROT);
		var->cam.dir.y = dx * sin(ROT) + dy * cos(ROT);
	}
	else if (var->event.key.keysym.sym == SDLK_UP
		|| var->event.key.keysym.sym == SDLK_DOWN)
		ft_arrow(var);
	return (0);
}
