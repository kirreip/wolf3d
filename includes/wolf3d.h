/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/30 02:46:31 by pmartin           #+#    #+#             */
/*   Updated: 2016/11/22 17:31:58 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H

# define WOLF3D_H
# include "../libft/includes/libft.h"
# include <SDL2/sdl.h>
# include <stdlib.h>
# include <fcntl.h>
# include <sys/types.h>
# include <math.h>
# include <float.h>
# include <stdio.h>
# define DIST 1
# define WIDTH 1.5
# define ROT 0.104716
# define FOR 0.3
# define NTM 1

typedef struct		s_3d
{
	double			x;
	double			y;
}					t_3d;

typedef struct		s_cam
{
	t_3d			o;
	t_3d			dir;
}					t_cam;
typedef struct		s_tool
{
	int				mapx;
	int				mapy;
	int				side;
	int				x;
	int				y;
	double			sx;
	double			sy;
	double			dx;
	double			dy;
	double			dist;
}					t_tool;
typedef struct		s_var
{
	int				sizex;
	int				sizey;
	t_tool			tool;
	int				x;
	int				y;
	int				imgx;
	int				imgy;
	SDL_Texture		*sdl_tex;
	SDL_Window		*sdl_win;
	SDL_Renderer	*sdl_ren;
	SDL_Event		event;
	SDL_Surface		*surface;
	char			map[21][60];
	t_cam			cam;
	t_3d			ray;
	t_3d			viewplane;
}					t_var;

double				ft_sq(double k);
int					ft_key_hook(t_var *var);
int					map(t_var *var, int x, int y);
void				init_map(t_var *var);
t_3d				ft_addvec3d(t_3d o, t_3d vec);
t_3d				ft_lenvec(t_3d vec, double l);
void				ft_ezvec(t_3d *vec, t_3d *a, t_3d *b);
void				ft_normvec(t_3d *vec);
void				init_dda(t_var *var);
void				ez_dda(t_var *var);
void				osef();
void				init_viewplane(t_var *var);
void				init_ray(t_var *var);
void				mainc(t_var *var);
void				ft_parser(t_var *var, char **av);
void				checkpars(char *s);
void				mainc2(t_var *var);
void				event_sdl(t_var *var);
void				init(t_var *var);
#endif
